package uj.java.pwj2019.kindergarten;

public class Fork {
    private boolean toUse;

    public Fork() {
        this.toUse = true;
    }

    public boolean getToUse() {
        return this.toUse;
    }

    public void setToUse(boolean val) {
        this.toUse = val;
    }
}