package uj.java.pwj2019.kindergarten;

public class MyChild extends Child implements Runnable {

    private int leftFork;
    private int rightFork;

    public MyChild(String name, int hungerSpeedMs, int leftFork, int rightFork) {
        super(name, hungerSpeedMs);
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (Teacher.canEat(leftFork) && Teacher.canEat(rightFork)) {
                    Teacher.forks[leftFork].setToUse(false);
                    Teacher.forks[rightFork].setToUse(false);
                    eat();
                    Teacher.forks[leftFork].setToUse(true);
                    Teacher.forks[rightFork].setToUse(true);
                    Thread.sleep(100);
                } else {
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
