package uj.java.pwj2019.kindergarten;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class Teacher {
    public static Fork[] forks;

    public Teacher(String path) throws IOException {
        List<String> listOfChildren = Files.readAllLines(Path.of(path));
        int numberOfChildren = Integer.parseInt(listOfChildren.get(0));

        createForks(numberOfChildren);
        createChild(listOfChildren, numberOfChildren);

    }

    private void createForks(int numberOfForks) {
        forks = new Fork[numberOfForks];
        for (int i = 0; i < numberOfForks; i++) {
            forks[i] = new Fork();
        }
    }

    private void createChild(List<String> listOfChildren, int numberOfChildren) {
        for (int i = 1; i <= numberOfChildren; i++) {
            String nameChild = listOfChildren.get(i).split(" ")[0];
            int hungerSpeedMs = Integer.parseInt(listOfChildren.get(i).split(" ")[1]);
            MyChild child = new MyChild(nameChild, hungerSpeedMs, i-1, i % numberOfChildren);
            Thread th = new Thread(child);
            th.start();
        }
    }

    public static synchronized boolean canEat(int index) {
        return forks[index].getToUse();
    }

}
